# Installation

To install the analysis toolkit, you need to create the database from the quators
information file. To do this, execute the `samples_db.py` script with the samples
information file as the only arguments.

e.g.

    $ ./samples_db.py ../complete_trios

The database will be filled with the informations from the `complete_trios` file
and the functions from the `samples_db` module will work to efficiently retrieve
information about families.
Examples are provided in the `sample_db_files` directory of this repo.

# Expected directory structure

The expected directory structure for CNV calls for a family is the following:

    family_1121/
    ├── LP6005057-DNA_E02
    │   └── calls
    ├── LP6005057-DNA_F01
    │   └── calls
    ├── LP6005058-DNA_A01
    │   └── calls
    └── LP6005058-DNA_B02
        └── calls

The ``create_family_directory_structure.sh`` script is essential to start from this structure
and generate a tree that will be usable by the analysis scripts.

The ``create_family_directory_structure.sh`` script takes a base directory and creates the
appropriate filenames (representative of the family status for each sample) by creating
symlinks. It uses the previously generated database for quick lookups.
Alternatively, the symlinks can be created manually, the only requirement being that in the family directory (`e.g.` ``family_1121``) subdirectories starting with the words `mother`, `father`, `twin1` and `twin2` exist. These "sample directories" need to have another subdirectory called ``calls`` which contains the raw output files from the considered tool (`e.g.` ERDS). This directory structure is not flexible, so feel free to contact me (my bitbucket username @gmail.com) with any questions.

# Scripts

Most scripts use the following generic syntax:

    script_name.py --base_dir ./path/to/family --format file_format

or

    script_name.py --pickle ./path/to/serialized_object.pickle --family family_id

In the following examples, the `base_dir` is the path to a family directory that
is expected to follow the guidelines describes in the `Expected directory
structure` section of this document.
The `format` argument will be used to parse the input files into a format that
will be easily managable by the python scripts.
The currently supported formats can be seen using the ``--help`` option when calling a script. It is fairly simple to add a new supported format by simply following the generic class from the ``parsers`` module's ``__init__.py`` file (called ``ParentParser``). The documentation provides a more detailed explanation of the functionality of this module.

Alternatively, the second example uses the pickled for of the loaded CNVs. This
is the format used when the scripts modify the CNV data. As an example, the
``merge_cnvs.py`` script is very useful to merge CNVs based on a distance
threshold. The merged CNVs are then written to a ``.pickle`` file containing all
the CNVs from the family. The other scripts can then load this file by using the
``--pickle ./path/to/pickle_file`` and manually setting the ``--family fam_id``
argument.

# Documentation

The documentation for this small toolbox is hosted on StatGen's website at [http://statgen.org/wp-content/uploads/Softwares/CNVAnalysisToolkit/docs/](http://statgen.org/wp-content/uploads/Softwares/CNVAnalysisToolkit/docs/).
