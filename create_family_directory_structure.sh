#!/bin/bash

# This script creates the directory architecture needed by the analysis scripts by
# doing the appropriate db lookups and creating symlinks that will be used by the
# other analysis scripts. 
# The original structure should be something like:
#
# family_1121/
# ├── LP6005057-DNA_E02
# │   └── calls
# ├── LP6005057-DNA_F01
# │   └── calls
# ├── LP6005058-DNA_A01
# │   └── calls
# └── LP6005058-DNA_B02
#     └── calls
#
# Where the calls folders have 1 `.cnvs` file per chromosome.
#
# Usage:
# ./create_family_directory_structure.sh base_path
#

BASE_PATH=$1

# Oh please don't judge me.
BASE_PATH=`python -c "import os.path as p; print p.abspath('${BASE_PATH}')"`

if [ -d $BASE_PATH ]; then

    echo "Treating $BASE_PATH"

else
    echo "Usage: "
    echo "./create_family_directory_structure.sh base_path"
    echo "Where base_path has the architecture described in the script file's header."
    exit

fi

for i in `find $BASE_PATH -depth 1 -type d -name "*"`; do
    # Lookup the sample in the database
    SAMPLE=`basename $i`
    STATUS=`echo "SELECT status FROM quatuors WHERE id='${SAMPLE}';" | sqlite3 samples.db`
    if [[ $STATUS ]]; then
        echo "Creating symlink for ${STATUS}"
        ln -s "${i}" "${BASE_PATH}/${STATUS}_${SAMPLE}"
    fi
done
