#!/usr/bin/env python2.7

import sys
import os.path

import parsers
import parsers.cnvpickle

# This file is part of CNVAnalysisToolkit.
# 
# CNVAnalysisToolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# CNVAnalysisToolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with CNVAnalysisToolkit.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Marc-Andre Legault (StatGen)"
__copyright__ = "Copyright (C) 2013 StatGen"
__license__ = "GNU General Public License v3 (GPL-3)"

def get_parser_if_valid(args):
    """Parses the command line arguments and returns a CNV parser if they are valid.

    Checks if ``--base_dir`` is a directory and has a corresponding ``--format``.
    Also verifies that a ``--family`` was given when using the ``--picke`` 
    argument.
    
    This function returns a :py:class:`parsers.ParentParser` subclass
    corresponding to the ``--format`` (or a pickle parser if using the
    ``--pickle`` option.

    """

    if args.base_dir:
        if not os.path.isdir(args.base_dir):
            sys.exit("The --base_dir should be the calls directory.")
        elif not args.format:
            sys.exit("You need to provide a --format.")

        p = parsers.get_parser_for_algorithm(args.format)
        # Initialize the Parser.
        p = p(args.base_dir)

    elif args.pickle:
        if not args.family:
            sys.exit("You need to manually provide the family id with --family.")
        p = parsers.cnvpickle.Parser
        p = p(args.pickle, args.family)

    else:
        sys.exit("Either provide a --pickle file or a --base_dir.")

    return p

def add_pickle_args(parser):
    parser.add_argument(
                            "--pickle",
                            type=str,
                            help=("Load CNVs from serialized pickle data.")
                       )

    parser.add_argument(
                            "--family",
                            type=str,
                            help="Family ID for pickled data."
                       )

    return parser

def add_dir_args(parser):
    parser.add_argument("--format",
                        type=str,
                        choices=parsers.__all__,
                        help="The file format for the parser.")

    parser.add_argument("--base_dir",
                        type=str,
                        help=("The base directory for the calls. A directory with "
                              "four subfolders starting with father, mother, twin1 "
                              "and twin2 are expected."))
    return parser
