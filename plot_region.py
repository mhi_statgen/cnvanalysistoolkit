#!/usr/bin/env python2.7

import argparse
import os
import re

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
try:
    import scipy.stats
    has_stats = True
except Exception:
    has_stats = False
    # Will uglily fallback to R.
    
import sh # Install the sh module.

# This file is part of CNVAnalysisToolkit.
# 
# CNVAnalysisToolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# CNVAnalysisToolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with CNVAnalysisToolkit.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Marc-Andre Legault (StatGen)"
__copyright__ = "Copyright (C) 2014 StatGen"
__license__ = "GNU General Public License v3 (GPL-3)"


def main(args):
    """
    Creates plots of coverage to manually verifiy CNVs in regions.

    """

    bam = os.path.abspath(args.bam)
    m = re.match(r"^(chr[0-9]+):([0-9]+)-([0-9]+)$", args.region)
    chrom = m.group(1)
    start = int(m.group(2))
    end = int(m.group(3))
    cnv_size = end - start

    if not args.surrounding:
        padding = int(round(0.3 * cnv_size))
    else:
        padding = int(args.surrounding)

    start -= padding
    end += padding

    if padding > 0:
        vlines = [padding, padding + cnv_size]
    else:
        vlines = None

    # Launch the pileup over the region of interest.
    cmd_region = "{}:{}-{}".format(chrom, start, end)
    cmd = ["mpileup", "-f", args.ref, "-r", cmd_region, bam]

    signal = []
    for line in sh.samtools(*cmd, _iter=True):
        line = line.rstrip("\n")
        line = line.split("\t")

        rel_pos = int(line[1]) - start
        depth = int(line[3])
        signal.append(depth)

    signal = np.array(signal)

    avg_signal = movingaverage(signal, args.window_size)

    if args.plot_cov_dist:
        # Check normality
        out_signal = np.concatenate(
            (signal[0:padding], signal[padding+cnv_size:])
        )
        in_signal = signal[padding+1:padding+cnv_size]

        fig = plt.figure()
        n, bins, _ = plt.hist(
            in_signal, color="#33B5E5", label="In region", bins=25, alpha=0.85,
            weights=np.zeros_like(in_signal) + 1. / in_signal.size
        )
        mu, sigma = (np.mean(in_signal), np.std(in_signal))
        y = normpdf(bins, mu, sigma)
        plt.plot(bins, y, 'k--', color="black", linewidth=0.5)

        n, bins, _ = plt.hist(
            out_signal, color="#99CC00", label="Out of region", bins=25, 
            alpha=0.85, weights=np.zeros_like(out_signal) + 1. / out_signal.size
        )
        mu, sigma = (np.mean(out_signal), np.std(out_signal))
        y = normpdf(bins, mu, sigma)
        plt.plot(bins, y, 'k--', color="black", linewidth=0.5)

        plt.legend()
        plt.savefig("cov_dist.png", dpi=300)

        # Compute t test statistic
        # Using Welch's t test
        t, p = scipy.stats.ttest_ind(in_signal, out_signal, equal_var=False)
        print "p-value for t-test", p

    fig = plt.figure()
    plt.scatter(np.arange(0, len(signal)), signal, s=1, facecolor='0.5', lw = 0)
    plt.plot(np.arange(0, len(avg_signal)), avg_signal, color="black")
    if vlines is not None:
        plt.axvline(x=vlines[0])
        plt.axvline(x=vlines[1])
    if args.save:
        if args.save.endswith(".png"):
            plt.savefig(args.save, dpi=300)
        else:
            plt.savefig(args.save)
    else:
        plt.show()


def normpdf(x, mu, sigma):
    u = (x-mu)/abs(sigma)
    y = (1/(np.sqrt(2*np.pi)*abs(sigma)))*np.exp(-u*u/2)
    return y


def movingaverage(interval, window_size):
    """http://stackoverflow.com/questions/11352047/finding-moving-average-from-data-points-in-python
    """

    window = np.ones(int(window_size))/float(window_size)
    return np.convolve(interval, window, 'same')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description = ("Plot the coverage in "
        "a given region."))

    parser.add_argument("--ref",
        help="A samtools compatible reference (fasta, faidx indexed).",
        required=True,
        type=str,
    )

    parser.add_argument("--bam",
        help="The bam file from which to plot the region.",
        required=True,
        type=str
    )

    parser.add_argument("--window_size",
        help=("Size of the windows for the smoothed average "
              "(default 100)."),
        default=100,
        type=int,
    )

    parser.add_argument("--region",
        help="Genomic region of interest. Of the form chrXX:START-END",
        required=True,
        type=str,
    )

    parser.add_argument("--surrounding",
        help=("Number of bins to take left and right "
              "(default is 30% of the number of number of bins in the "
              "region)."),
        default=None,
        type=int
    )

    parser.add_argument("--plot_cov_dist",
        help=("Plot coverage distributions to check normality."),
        action="store_true"
    )

    parser.add_argument("--save",
                    type=str,
                    help="Filename of the output graph file.",
                    default=None
                   )

    main(parser.parse_args())

