# Parser for CNVer output CNV calls.

import os
import pickle

import cnv_struct
import parsers

# This file is part of CNVAnalysisToolkit.
# 
# CNVAnalysisToolkit is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# CNVAnalysisToolkit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with CNVAnalysisToolkit.  If not, see <http://www.gnu.org/licenses/>.

__author__ = "Marc-Andre Legault (StatGen)"
__copyright__ = "Copyright (C) 2013 StatGen"
__license__ = "GNU General Public License v3 (GPL-3)"

class Parser(parsers.ParentParser):
    """Loads CNVs from pickled versions. 
    
    In contrast to the other methods, this parser need an explicit family_id, 
    because it cannot be inferred from the directory structure.

    This is a glorified ``pickle.load``.

    .. note::
        The format of the container structure will not be enforced. No warning
        will be emitted.

    """

    def __init__(self, fn, family_id):
        self.family_id = family_id
        self.fn = fn
        with open(self.fn) as f:
            self.cnvs = pickle.load(f)


    def get_cnvs(self):
        return self.cnvs
